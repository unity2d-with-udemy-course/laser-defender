﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private List<WaveConfig> waveConfigs;
    [SerializeField] private int startingWave = 0;
    [SerializeField] private bool looping = false;
    
    
    // Start is called before the first frame update
    IEnumerator Start()
    {
        do
        {
            yield return StartCoroutine(SpawnAllWaves());
        } while (looping);
    }

    private IEnumerator SpawnAllEnemiesInWave(WaveConfig currentWaveConfig)
    {
        for (int i = 0; i < currentWaveConfig.NumberOfEnemies; i++)
        {
            var newEnemy = Instantiate(currentWaveConfig.EnemyPrefab,
                currentWaveConfig.GetWaypoints()[0].transform.position, Quaternion.identity);
            newEnemy.GetComponent<EnemyPathing>().WaveConfig = currentWaveConfig;
            yield return new WaitForSeconds(currentWaveConfig.TimeBetweenSpawns);
        }
    }

    private IEnumerator SpawnAllWaves()
    {
        for (int indexWave = startingWave; indexWave < waveConfigs.Count; indexWave++)
        {
            yield return StartCoroutine(SpawnAllEnemiesInWave(waveConfigs[indexWave]));
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
