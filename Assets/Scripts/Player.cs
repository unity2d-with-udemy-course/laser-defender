﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;

public class Player : MonoBehaviour
{
    // configuration parameters
    [SerializeField] private GameObject background;
    [Header("Player")]
    [SerializeField] float moveSpeed = 10f;
    [SerializeField] private float padding = 0.5f;
    [SerializeField] private int health = 200;
    
    [Header("Sound Effects")]
    [SerializeField] private AudioClip gameOverSound;
    [SerializeField] private AudioClip laserSound;
    [SerializeField] [Range(0,2)] private float laserVolume = 1f;
    [SerializeField] [Range(0,2)] private float breakVolume = 1f;
    
    [Header("Projectile")]
    [SerializeField] private GameObject laserPrefab;
    [SerializeField] private float projectileSpeed = 10f;
    [SerializeField] private float projectileFiringPeriod = 0.1f;

    private Coroutine firingCoroutine;
    
    private float xMin;
    private float xMax;
    private float yMin;
    private float yMax;
    
    // Start is called before the first frame update
    void Start()
    {
        SetUpMoveBoundaries();
    }

    public int Health => health;

    private void SetUpMoveBoundaries()
    {
        // j'ai changé xMin et xMax pour le passage de 9:16 à 1920x1080
        Camera gameCamera = Camera.main;
        //xMin = gameCamera.ViewportToWorldPoint(new Vector3(0, 0, 0)).x + padding;
        xMin = - background.transform.lossyScale.x / 2 + padding;
        //xMax = gameCamera.ViewportToWorldPoint(new Vector3(1, 0, 0)).x - padding;
        yMin = gameCamera.ViewportToWorldPoint(new Vector3(0, 0, 0)).y + padding;
        xMax = background.transform.lossyScale.x / 2 - padding;
        yMax = gameCamera.ViewportToWorldPoint(new Vector3(0, 1, 0)).y - padding;
        
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        Fire();
    }

    private void Fire()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            firingCoroutine = StartCoroutine(FireContinuously());
        }
        if (Input.GetButtonUp("Fire1"))
        {
            StopCoroutine(firingCoroutine);
        }
    }

    private void Move()
    {
        var deltaX = Input.GetAxis("Horizontal") * Time.deltaTime * moveSpeed;
        var deltaY = Input.GetAxis("Vertical") * Time.deltaTime * moveSpeed;
        var newXPos = transform.position.x + deltaX;
        var newYPos = transform.position.y + deltaY;
        newXPos = Mathf.Clamp(newXPos, xMin, xMax);
        newYPos = Mathf.Clamp(newYPos, yMin, yMax);
        transform.position = new Vector2(newXPos, newYPos);
    }

    private IEnumerator FireContinuously()
    {
        while (true)
        {
            AudioSource.PlayClipAtPoint(laserSound, Camera.main.transform.position, laserVolume);
            GameObject laser = Instantiate(laserPrefab, transform.position, Quaternion.identity);
            laser.GetComponent<Rigidbody2D>().velocity = new Vector2(0, projectileSpeed);
            yield return new WaitForSeconds(projectileFiringPeriod);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {

        DamageDealer damageDealer = other.gameObject.GetComponent<DamageDealer>();
        if (!damageDealer) return;
        ProcessHit(damageDealer);
    }
    
    private void ProcessHit(DamageDealer damageDealer)
    {
        health -= damageDealer.Damage;
        damageDealer.Hit();
        if (health <= 0)
        {
            health = 0;
            AudioSource.PlayClipAtPoint(gameOverSound, Camera.main.transform.position, breakVolume);
            Destroy(gameObject);
            
            FindObjectOfType<Level>().LoadGameOver();
        }
    }
}
