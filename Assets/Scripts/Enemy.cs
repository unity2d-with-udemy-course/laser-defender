﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;
using Random = UnityEngine.Random;

public class Enemy : MonoBehaviour
{
    [Header("Enemy Stats")]
    [SerializeField] private int health = 100;
    [SerializeField] private int pointValue = 50;
    
    [Header("Shots")]
    private float shotCounter;
    [SerializeField] private float minTimeBetweenShots = 0.2f;
    [SerializeField] private float maxTimeBetweenShots = 3f;
    [SerializeField] private GameObject laserPrefab;
    [SerializeField] private float projectileSpeed = -10f;
    
    [SerializeField] private GameObject explosionVFX;
    [SerializeField] private float durationOfExplosion = 1f;

    [Header("Bonus Drops")] [SerializeField]
    private GameObject bonusViePrefab;
    [SerializeField] private float bonusSpeed = -3f;
    [SerializeField] private float probaDropBonus = 0.5f;
    private float randomDrop;

    [Header("Sound Effects")]
    [SerializeField] private AudioClip breakSound;
    [SerializeField] private AudioClip laserSound;
    [SerializeField] private AudioClip bonusSound;
    [SerializeField] [Range(0,2)] private float laserVolume = 1f;
    [SerializeField] [Range(0,2)] private float breakVolume = 1f;
    [SerializeField] [Range(0,2)] private float bonusVolume = 1f;

    private GameSession gameSession;
    
    // Start is called before the first frame update
    void Start()
    {
        gameSession = FindObjectOfType<GameSession>();
        shotCounter = Random.Range(minTimeBetweenShots, maxTimeBetweenShots);
    }

    // Update is called once per frame
    void Update()
    {
        CountDownAndShoot();
    }

    public int PointValue => pointValue;

    private void CountDownAndShoot()
    {
        shotCounter -= Time.deltaTime;
        if (shotCounter <= 0)
        {
            Fire();
            shotCounter = Random.Range(minTimeBetweenShots, maxTimeBetweenShots);
        }
    }

    private void Fire()
    {
        AudioSource.PlayClipAtPoint(laserSound, Camera.main.transform.position, laserVolume);
        GameObject laser = Instantiate(laserPrefab, transform.position, Quaternion.identity);
        laser.GetComponent<Rigidbody2D>().velocity = new Vector2(0, projectileSpeed);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        DamageDealer damageDealer = other.gameObject.GetComponent<DamageDealer>();
        if (!damageDealer) return;
        ProcessHit(damageDealer);
    }

    private void ProcessHit(DamageDealer damageDealer)
    {
        health -= damageDealer.Damage;
        damageDealer.Hit();
        if (health <= 0)
        {
            Die();
        }
    }

    public void Die()
    {
        randomDrop = Random.Range(0f, 1f);
        if (randomDrop <= probaDropBonus)
        {
            GameObject bonus = Instantiate(bonusViePrefab, transform.position, Quaternion.identity);
            bonus.GetComponent<Rigidbody2D>().velocity = new Vector2(0, bonusSpeed);
            AudioSource.PlayClipAtPoint(bonusSound, Camera.main.transform.position, bonusVolume);
        }
        gameSession.AddToScore(pointValue);
        Destroy(gameObject);
        AudioSource.PlayClipAtPoint(breakSound, Camera.main.transform.position, breakVolume);
        GameObject explosion = Instantiate(explosionVFX, transform.position, Quaternion.identity);
        Destroy(explosion, durationOfExplosion);
    }
}
