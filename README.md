A little game of Laser Defender that I have done with a Udemy class.

The sound effects and sprites are from Kenney's web site : https://kenney.nl/

The music of my game is from the recently open sourced Monster RPG 2 of Nooskewl Games.